package maestro.glfilters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import java.io.File;

/**
 * Created by maestro123 on 8/7/16.
 */
public class ImageViewer extends FragmentActivity {

    private ImageView mImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview);

        mImage = (ImageView) findViewById(R.id.image);

        File file = new File(Environment.getExternalStorageDirectory(), "temp.jpg");

        Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());

        mImage.setImageBitmap(bitmap);

    }

}
