package maestro.glfilters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import maestro.glfilters.core.AndroidEffectsProvider;
import maestro.glfilters.core.GLImageRenderer;
import maestro.glfilters.core.GLImageView;
import maestro.glfilters.core.GPUEffectsProvider;
import maestro.glfilters.core.PreviewLoader;
import maestro.glfilters.core.SourceProvider;

public class MainActivity extends AppCompatActivity {

    private GLImageView mSurface;
    private ImageView mResultImage;
    private ProgressBar mProgress;

    private RecyclerView mList;

    private AndroidEffectsProvider mEffectsProvider;

    private float mScale = 1f;
    private float mStep = 0.5f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSurface = (GLImageView) findViewById(R.id.surface);
        mProgress = (ProgressBar) findViewById(R.id.progress);

        mList = (RecyclerView) findViewById(R.id.list);

        mSurface.setOnSourceLoadEventListener(new SourceProvider.OnSourceLoadEvent() {
            @Override
            public void onSourceLoad(Bitmap bitmap) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgress.setVisibility(View.GONE);
                    }
                });
            }

            @Override
            public void onSourceLoadFail(Exception e) {

            }
        });

        final int source = R.drawable.main_900;

        final SourceProvider sourceProvider = new SourceProvider() {
            @Override
            protected Bitmap obtain(Context context) throws Exception {
                return BitmapFactory.decodeResource(context.getResources(), source);
            }
        };
        mEffectsProvider = new AndroidEffectsProvider();

        mSurface.setup(sourceProvider, mEffectsProvider);

        mList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
        mList.setAdapter(new PreviewAdapter(getApplicationContext(), String.valueOf(source), sourceProvider, AndroidEffectsProvider.getAvailableEffects()));

//        findViewById(R.id.result).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//                mSurface.getResult(new GLImageRenderer.OnStoreListener() {
//                    @Override
//                    public void onStore(Bitmap bitmap) {
//                        File file = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
//                        try {
//                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                startActivity(new Intent(view.getContext(), ImageViewer.class));
//                            }
//                        });
//                    }
//                });
//            }
//        });

        findViewById(R.id.copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mSurface.getResult(new GLImageRenderer.OnStoreListener() {
                    @Override
                    public void onStore(Bitmap bitmap) {
                        File file = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                        try {
                            file.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        Log.e("TAG", "done");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("TAG", "done");
                                startActivity(new Intent(view.getContext(), ImageViewer.class));
                            }
                        });
                    }
                });
            }
        });

    }

    private class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.PreviewHolder> {

        private ArrayList<String> mEffects;
        private LayoutInflater inflater;

        private String mSource;

        private SourceProvider mSourceProvider;

        private PreviewLoader loader;

        public PreviewAdapter(Context context, String source, SourceProvider sourceProvider, ArrayList<String> effects) {
            mEffects = effects;
            mSourceProvider = sourceProvider;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            loader = new PreviewLoader(context);
            mSource = source;
        }

        @Override
        public PreviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new PreviewHolder(inflater.inflate(R.layout.image_preview, null));
        }

        @Override
        public void onBindViewHolder(PreviewHolder holder, int position) {
            holder.load(mEffects.get(position));
        }

        @Override
        public int getItemCount() {
            return mEffects != null ? mEffects.size() : 0;
        }

        class PreviewHolder extends RecyclerView.ViewHolder {

            ImageView Image;

            public PreviewHolder(View itemView) {
                super(itemView);
                Image = (ImageView) itemView.findViewById(R.id.image);
            }

            public void load(final String effect) {
                AndroidEffectsProvider provider = new AndroidEffectsProvider();
                provider.addEffect(effect);


                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mEffectsProvider.clear();
                        mEffectsProvider.addEffect(effect);
                        mSurface.update();

                        Log.e("TEST", "apply effect: " + effect);
                    }
                });

                loader.load(Image, mSource, effect, mSourceProvider, provider, new PreviewLoader.LoadListener() {
                    @Override
                    public void onLoadStart() {

                    }

                    @Override
                    public void onLoadFinished(Bitmap bitmap) {

                    }
                });
            }

        }

    }

}
