package maestro.glfilters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by maestro123 on 05.08.2016.
 */
public class NearLoader {

    private static volatile NearLoader instance;

    public static synchronized NearLoader get() {
        if (instance == null) {
            synchronized (NearLoader.class) {
                if (instance == null) {
                    instance = new NearLoader();
                }
            }
        }
        return instance;
    }

    private Bitmap mSource;
    private LoadThread mLoadThread;
    private OnSourceLoadListener mListener;

    public void setSourceLoadListener(OnSourceLoadListener listener) {
        mListener = listener;
    }

    public void loadSource(Context context, int id) {
        if (mLoadThread != null) {
            mLoadThread.cancel();
            if (mSource != null && !mSource.isRecycled()) {
                mSource.recycle();
                mSource = null;
            }
        }
        mLoadThread = new LoadThread(context, id);
    }

    private void onSourceLoad(Bitmap source) {
        if (source != null) {
            mSource = source;
            mListener.onSourceLoad(source);
        } else {
            mListener.onSourceLoadFail(null);
        }
    }

    public interface OnSourceLoadListener {
        void onSourceLoad(Bitmap bitmap);

        void onSourceLoadFail(Exception e);
    }

    private class LoadThread extends Thread {

        private Context mContext;
        private int mSource;

        private boolean isCanceled;

        public LoadThread(Context context, int source) {
            mContext = context;
            mSource = source;
        }

        @Override
        public void run() {
            super.run();
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mSource);
            if (!isCanceled) {
                NearLoader.get().onSourceLoad(bitmap);
            }
        }

        public void cancel() {
            isCanceled = true;
            interrupt();
        }

    }

}
