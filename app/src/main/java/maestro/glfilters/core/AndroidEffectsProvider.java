package maestro.glfilters.core;

import android.graphics.Color;
import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;
import android.util.Log;

import java.util.ArrayList;


/**
 * Created by maestro123 on 28.04.2016.
 */
public class AndroidEffectsProvider implements IEffectsProvider {

    public static final String TAG = AndroidEffectsProvider.class.getSimpleName();

    private static final String[] EFFECTS = {

            EffectFactory.EFFECT_AUTOFIX,
            EffectFactory.EFFECT_BLACKWHITE,
            EffectFactory.EFFECT_CROSSPROCESS,
            EffectFactory.EFFECT_CONTRAST,
            EffectFactory.EFFECT_SEPIA,
            EffectFactory.EFFECT_TEMPERATURE,
            EffectFactory.EFFECT_DOCUMENTARY,
            //EffectFactory.EFFECT_DUOTONE,
            EffectFactory.EFFECT_FILLLIGHT,
            //EffectFactory.EFFECT_FISHEYE,
            EffectFactory.EFFECT_GRAIN,
            EffectFactory.EFFECT_GRAYSCALE,
            EffectFactory.EFFECT_LOMOISH,
            EffectFactory.EFFECT_BRIGHTNESS,
            //EffectFactory.EFFECT_NEGATIVE,
            EffectFactory.EFFECT_POSTERIZE,
            EffectFactory.EFFECT_SATURATE,
            EffectFactory.EFFECT_SHARPEN,
            EffectFactory.EFFECT_TINT,
            //EffectFactory.EFFECT_VIGNETTE
    };

    private static final ArrayList<String> AVAILABLE_EFFECTS = new ArrayList<>();

    static {
        for (String effect : EFFECTS) {
            if (EffectFactory.isEffectSupported(effect)) {
                AVAILABLE_EFFECTS.add(effect);
            }
        }
    }

    public static ArrayList<String> getAvailableEffects() {
        return (ArrayList<String>) AVAILABLE_EFFECTS.clone();
    }

    private ArrayList<String> mEffects = new ArrayList<>();
    private EffectFactory mEffectFactory;

    @Override
    public void addEffect(String effect) {
        mEffects.add(effect);
    }

    @Override
    public void clear() {
        mEffects.clear();
    }

    @Override
    public void draw(GLTexHolder holder, int originTexture, int outTexture, int sourceWidth, int sourceHeight) {
        if (mEffectFactory == null) {
            mEffectFactory = EffectContext.createWithCurrentGlContext().getFactory();
        }
        boolean applyOver = false;
        for (String effectId : mEffects) {
            Effect effect = initEffect(null, mEffectFactory, effectId);

            if (effect == null) {
                Log.e(TAG, "null effect !!!: " + effectId);
                continue;
            }
            Log.e(TAG, "applyEffect: " + effectId);

            effect.apply(applyOver ? outTexture : originTexture, sourceWidth, sourceHeight, outTexture);
            effect.release();
            applyOver = true;
        }
    }

    private static volatile Object mLock = new Object();

    @Override
    public boolean haveEffects() {
        return mEffects.size() > 0;
    }

    public static synchronized Effect initEffect(Effect previous, EffectFactory effectFactory, String effect) {
        if (previous != null) {
            previous.release();
        }
        Effect mEffect = null;

        switch (effect) {

            case EffectFactory.EFFECT_AUTOFIX:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_AUTOFIX);
                mEffect.setParameter("scale", 0.5f);
                break;

            case EffectFactory.EFFECT_BLACKWHITE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BLACKWHITE);
                mEffect.setParameter("black", .1f);
                mEffect.setParameter("white", .7f);
                break;

            case EffectFactory.EFFECT_BRIGHTNESS:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BRIGHTNESS);
                mEffect.setParameter("brightness", 2.0f);
                break;

            case EffectFactory.EFFECT_CONTRAST:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_CONTRAST);
                mEffect.setParameter("contrast", 1.4f);
                break;

            case EffectFactory.EFFECT_CROSSPROCESS:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_CROSSPROCESS);
                break;

            case EffectFactory.EFFECT_DOCUMENTARY:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_DOCUMENTARY);
                break;

            case EffectFactory.EFFECT_DUOTONE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_DUOTONE);
                mEffect.setParameter("first_color", Color.YELLOW);
                mEffect.setParameter("second_color", Color.DKGRAY);
                break;

            case EffectFactory.EFFECT_FILLLIGHT:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FILLLIGHT);
                mEffect.setParameter("strength", .8f);
                break;

            case EffectFactory.EFFECT_FISHEYE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FISHEYE);
                mEffect.setParameter("scale", .5f);
                break;

            case EffectFactory.EFFECT_GRAIN:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_GRAIN);
                mEffect.setParameter("strength", 0.5f);
                break;

            case EffectFactory.EFFECT_GRAYSCALE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_GRAYSCALE);
                break;

            case EffectFactory.EFFECT_LOMOISH:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_LOMOISH);
                break;

            case EffectFactory.EFFECT_NEGATIVE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_NEGATIVE);
                break;

            case EffectFactory.EFFECT_POSTERIZE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_POSTERIZE);
                break;

            case EffectFactory.EFFECT_SATURATE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SATURATE);
                mEffect.setParameter("scale", .6f);
                break;

            case EffectFactory.EFFECT_SEPIA:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SEPIA);
                break;

            case EffectFactory.EFFECT_SHARPEN:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SHARPEN);
                mEffect.setParameter("scale", 0.5f);
                break;

            case EffectFactory.EFFECT_TEMPERATURE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TEMPERATURE);
                mEffect.setParameter("scale", .9f);
                break;

            case EffectFactory.EFFECT_TINT:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TINT);
                mEffect.setParameter("tint", Color.parseColor("#d83649"));
                break;

            case EffectFactory.EFFECT_VIGNETTE:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_VIGNETTE);
                mEffect.setParameter("scale", .5f);
                break;

            default:
                break;
        }
        return mEffect;
    }

}
