package maestro.glfilters.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.net.URL;
import java.net.URLConnection;

/**
 * Created by maestro123 on 8/21/16.
 */
public class BitmapSourceProvider extends SourceProvider {

    private String mSource;

    public BitmapSourceProvider(String source) {
        mSource = source;
    }

    @Override
    public Bitmap obtain(Context context) throws Exception {
        if (mSource.startsWith("https:") || mSource.startsWith("http:")) {
            URL url = new URL(mSource);
            URLConnection connection = url.openConnection();
            return BitmapFactory.decodeStream(connection.getInputStream());
        } else {
            Uri uri = Uri.parse(mSource);
            return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(uri));
        }
    }
}
