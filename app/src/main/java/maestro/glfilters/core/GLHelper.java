package maestro.glfilters.core;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

import java.nio.ByteBuffer;

/**
 * Created by maestro123 on 8/19/16.
 */
public class GLHelper {

    public static final String TAG = GLHelper.class.getSimpleName();

    public static void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            throw new RuntimeException(op + ": glError " + error + " - " + GLU.gluErrorString(error));
        }
    }

    public static void copyTexToTex(int originTexture, int outTexture, int width, int height) {
        int[] buffer = new int[1];
        GLES20.glGenFramebuffers(1, buffer, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, buffer[0]);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, originTexture, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, outTexture);
        GLES20.glCopyTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, 0, 0, width, height, 0);
        GLHelper.checkGlError("glCopyTexImage2D");
//        GLES20.glCopyTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, 0, 0, width, height);
//        GLHelper.checkGlError("glCopyTexSubImage2D");
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
    }

    public static Bitmap toBitmap(int texture, int width, int height) {
        int[] frame = new int[1];
        GLES20.glGenFramebuffers(1, frame, 0);
        checkGlError("glGenFramebuffers");
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frame[0]);
        checkGlError("glBindFramebuffer");
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                GLES20.GL_TEXTURE_2D, texture, 0);
        checkGlError("glFramebufferTexture2D");
        ByteBuffer buffer = ByteBuffer.allocate(width * height * 4);
        GLES20.glReadPixels(0, 0, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);
        checkGlError("glReadPixels");
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        checkGlError("glBindFramebuffer");
        GLES20.glDeleteFramebuffers(1, frame, 0);
        checkGlError("glDeleteFramebuffer");
        return bitmap;
    }

    public static Bitmap saveTexture(GLTexHolder holder, int texture, int originWidth, int originHeight) {
        int[] frame = new int[1];
        GLES20.glGenFramebuffers(1, frame, 0);
        checkGlError("glGenFramebuffers");
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frame[0]);
        checkGlError("glBindFramebuffer");
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, texture, 0);
        checkGlError("glFramebufferTexture2D");

        final float scale = holder.getTotalScale();

//        final float left = holder.getLeft(); //-1 is normal
        float leftOffset = -1 - holder.getLeft();

//        final float right = holder.getRight(); //1 is normal
        float rightOffset = holder.getRight() - 1;

//        final float top = holder.getTop(); //1 is normal
        float topOffset = holder.getTop() - 1;

//        final float bottom = holder.getBottom();//- 1 is normal
        float bottomOffset = -1 - holder.getBottom();

        leftOffset /= scale;
        rightOffset /= scale;
        topOffset /= scale;
        bottomOffset /= scale;

        int leftOffsetPx = (int) (originWidth / 2 * leftOffset);
        int rightOffsetPx = (int) (originWidth / 2 * rightOffset);
        int topOffsetPx = (int) (originHeight / 2 * topOffset);
        int bottomOffsetPx = (int) (originHeight / 2 * bottomOffset);

        int width = originWidth - leftOffsetPx - rightOffsetPx;
        int height = originHeight - topOffsetPx - bottomOffsetPx;

        Log.e("TEST", "offsetLeft: " + leftOffset + ", offsetRight: " + rightOffset + ", offsetTop: " + topOffset + ", offsetBottom: " + bottomOffset);

        long startTime = System.currentTimeMillis();

        ByteBuffer buffer = ByteBuffer.allocate(width * height * 4);
        GLES20.glReadPixels(leftOffsetPx, topOffsetPx, width, height, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, buffer);
        checkGlError("glReadPixels");
        Log.e(TAG, "readPixels time: " + (System.currentTimeMillis() - startTime));
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(buffer);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        checkGlError("glBindFramebuffer");
        GLES20.glDeleteFramebuffers(1, frame, 0);
        checkGlError("glDeleteFramebuffer");
        return bitmap;
    }

    public static Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }

    public static void renderBackground() {
        GLES20.glClearColor(0, 0, 0, 1);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
    }
}
