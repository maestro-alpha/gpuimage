package maestro.glfilters.core;

import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.util.Log;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by maestro123 on 8/4/16.
 */
public class GLImageRenderer implements GLSurfaceView.Renderer {

    public static final String TAG = GLImageRenderer.class.getSimpleName();

    private GLSurfaceView mSurface;

    private GLTexHolder mHolder;
    private GLTexHolder mDrawHolder;

    private IEffectsProvider mEffectsProvider;

    private int mTexture;
    private int mRenderTexture;
    private int mDisplayTexture;

    private int mBuffer;

    private int mWidth;
    private int mHeight;

    private int mSourceWidth;
    private int mSourceHeight;

    private float mScale = 1f;
    private float mXOffset = 0;
    private float mYOffset = 0;

    private Bitmap mSource;
    private OnStoreListener mStoreListener;

    boolean isChanged = true;

    public GLTexHolder getDrawHolder() {
        return mDrawHolder;
    }

    public interface OnStoreListener {
        void onStore(Bitmap bitmap);
    }

    public GLImageRenderer(GLSurfaceView surfaceView) {
        if (surfaceView != null) {
            mSurface = surfaceView;
            mSurface.setEGLContextClientVersion(2);

            mSurface.setRenderer(this);
            mSurface.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        }
    }

    public void setEffectsProvider(IEffectsProvider provider) {
        mEffectsProvider = provider;
        requestRenderer();
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig eglConfig) {
        mHolder = GLTexHolder.create();
        mDrawHolder = GLTexHolder.create();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    @Override
    public void onDrawFrame(GL10 gl) {

        long startTime = System.currentTimeMillis();

        GLHelper.renderBackground();

        if (mSource != null) {
            if (mTexture == 0) {
                final int[] textures = new int[3]; //0 - origin, 1 - with effects, 2 - for display
                GLES20.glGenTextures(3, textures, 0);

                mTexture = textures[0];
                mRenderTexture = textures[1];
                mDisplayTexture = textures[2];

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTexture);
                setTexParams();
                GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, mSource, 0);

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mRenderTexture);
                setTexParams();
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mSourceWidth, mSourceHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mDisplayTexture);
                setTexParams();
                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mSourceWidth, mSourceHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

                mHolder.setViewPortSize(mWidth, mHeight);
                mDrawHolder.setViewPortSize(mWidth, mHeight);

                mHolder.setTargetSize(mSourceWidth, mSourceHeight, false);
                mDrawHolder.setTargetSize(mSourceWidth, mSourceHeight, true);

                prepareBuffer();

                prepareRenderBuffer();
            }

            if (isChanged) {

                Log.e(TAG, "effectProvider: " + mEffectsProvider);

                final int textureForCopy;
                if (mEffectsProvider == null || !mEffectsProvider.haveEffects()) {
//                    GLTexHolder.renderTexture(mDrawHolder, mTexture, true);
                    textureForCopy = mTexture;
                } else {
                    mEffectsProvider.draw(mHolder, mTexture, mRenderTexture, mSourceWidth, mSourceHeight);
                    textureForCopy = mRenderTexture;
                }

                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mBuffer);
                GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0,
                        GLES20.GL_TEXTURE_2D, textureForCopy, 0);

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mDisplayTexture);
                GLES20.glCopyTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, 0, 0, mSourceWidth, mSourceHeight, 0);
                GLHelper.checkGlError("glCopyTexImage2D");

                GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
                isChanged = false;
            }

            GLTexHolder.renderTexture(mDrawHolder, mDisplayTexture, false);

            if (mStoreListener != null) {
                mStoreListener.onStore(GLHelper.saveTexture(mDrawHolder, mDisplayTexture, mSourceWidth, mSourceHeight));
                mStoreListener = null;
            }
        }

        Log.e(TAG, "drawTime: " + (System.currentTimeMillis() - startTime) + ", source: " + mSource);
    }

    public int getDisplayTexture() {
        return mDisplayTexture;
    }

    private void prepareBuffer() {
        int[] buffer = new int[1];
        GLES20.glGenFramebuffers(1, buffer, 0);
        mBuffer = buffer[0];
    }

    public void getResult(OnStoreListener listener) {
        mStoreListener = listener;
        requestRenderer();
    }

    public void setSource(Bitmap source) {
        mSource = source;
        mSourceWidth = source.getWidth();
        mSourceHeight = source.getHeight();
        requestRenderer();
    }

    public void setScale(float scale) {
        mScale = scale;
        mDrawHolder.scale(scale);
        requestRenderer();
    }

    public void translate(float xOffset, float yOffset) {
        mXOffset += xOffset;
        mYOffset += yOffset;
        mDrawHolder.translate(mXOffset, mYOffset);
        requestRenderer();
    }

    public void update() {
        isChanged = true;
        requestRenderer();
    }

    private void requestRenderer() {
        if (mSurface != null) {
            mSurface.requestRender();
        }
    }

    public float getLeft() {
        return mDrawHolder.getLeft();
    }

    public float getRight() {
        return mDrawHolder.getRight();
    }

    public float getTop() {
        return mDrawHolder.getTop();
    }

    public float getBottom() {
        return mDrawHolder.getBottom();
    }

    public boolean isOutOfLeft() {
        return mDrawHolder.posVertices.get(0) < -1f;
    }

    public boolean isOutOfRight() {
        return mDrawHolder.posVertices.get(2) > 1f;
    }

    public boolean isOutOfTop() {
        return mDrawHolder.posVertices.get(5) > 1f;
    }

    public boolean isOutOfBottom() {
        return mDrawHolder.posVertices.get(3) < -1f;
    }

    private static void setTexParams() {
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
    }

    private int mFrameBuffer;
    private int mRenderBuffer;

    private void prepareRenderBuffer() {
        int[] buffer = new int[1];

        GLES20.glGenFramebuffers(1, buffer, 0);
        mFrameBuffer = buffer[0];

        GLES20.glGenRenderbuffers(1, buffer, 0);
        mRenderBuffer = buffer[0];

        GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, mRenderBuffer);
        GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, GLES20.GL_DEPTH_COMPONENT16, mSourceWidth, mSourceHeight);

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffer);
        GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, mDisplayTexture, 0);
        GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER, GLES20.GL_DEPTH_ATTACHMENT, GLES20.GL_RENDERBUFFER, mRenderBuffer);

        int bufferStatus = GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER);
        if (bufferStatus != GLES20.GL_FRAMEBUFFER_COMPLETE) {
            Log.e(TAG, "Framebuffer status: " + bufferStatus);
        } else {
            Log.e(TAG, "Framebuffer status: OK");
        }

        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, 0);
    }

}