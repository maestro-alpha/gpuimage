package maestro.glfilters.core;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Created by maestro123 on 8/5/16.
 */
public class GLImageView extends GLSurfaceView implements SourceProvider.OnSourceLoadEvent {

    public static final String TAG = GLImageView.class.getSimpleName();

    private GLImageRenderer mRenderer;

    private static final float MAX_SCALE = 3f;
    private static final float MIN_SCALE = 1f;

    private static final float MIN_OVER_SCALE = 0.2f;
    private static final float MAX_OVER_SCALE = 3.5f;

    public GLImageView(Context context) {
        super(context);
        init();
    }

    public GLImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private SourceProvider mSourceProvider;

    private ScaleGestureDetector mScaleDetector;

    private MotionEvent mCurrentDownEvent;
    private MotionEvent mPreviousUpEvent;

    private FlingRunnable mFlingRunnable;
    private VelocityTracker mVelocityTracker;
    private SourceProvider.OnSourceLoadEvent mExtraSourceEventListener;

    private float mTouchSlop;
    private float mTouchSlopSquare;

    private float mLastFocusX;
    private float mLastFocusY;
    private float mDownFocusX;
    private float mDownFocusY;

    private int mMinimumFlingVelocity;
    private int mMaximumFlingVelocity;

    private boolean isScrolling;

    void init() {

        mRenderer = new GLImageRenderer(this);
        mScaleDetector = new ScaleGestureDetector(getContext(), mScaleListener);

        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        mTouchSlop = viewConfiguration.getScaledTouchSlop();
        mTouchSlopSquare = mTouchSlop * mTouchSlop;
        mMinimumFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
        mMaximumFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
    }

    public void update() {
        mRenderer.update();
    }

    public void setup(SourceProvider provider, IEffectsProvider effectsProvider) {
        mSourceProvider = provider;
        mRenderer.setEffectsProvider(effectsProvider);

        provider.load(getContext(), this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        mScaleDetector.onTouchEvent(ev);

        final int action = ev.getAction();

        if (mVelocityTracker == null) {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(ev);

        final boolean pointerUp =
                (action & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_UP;
        final int skipIndex = pointerUp ? ev.getActionIndex() : -1;

        // Determine focal point
        float sumX = 0, sumY = 0;
        final int count = ev.getPointerCount();
        for (int i = 0; i < count; i++) {
            if (skipIndex == i) continue;
            sumX += ev.getX(i);
            sumY += ev.getY(i);
        }
        final int div = pointerUp ? count - 1 : count;
        final float focusX = sumX / div;
        final float focusY = sumY / div;

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_POINTER_DOWN: {
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                break;
            }
            case MotionEvent.ACTION_POINTER_UP: {
                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;

                mVelocityTracker.computeCurrentVelocity(1000, mMaximumFlingVelocity);
                final int upIndex = ev.getActionIndex();
                final int id1 = ev.getPointerId(upIndex);
                final float x1 = mVelocityTracker.getXVelocity(id1);
                final float y1 = mVelocityTracker.getYVelocity(id1);
                for (int i = 0; i < count; i++) {
                    if (i == upIndex) continue;

                    final int id2 = ev.getPointerId(i);
                    final float x = x1 * mVelocityTracker.getXVelocity(id2);
                    final float y = y1 * mVelocityTracker.getYVelocity(id2);

                    final float dot = x + y;
                    if (dot < 0) {
                        mVelocityTracker.clear();
                        break;
                    }
                }
                break;
            }
            case MotionEvent.ACTION_DOWN: {

                mDownFocusX = mLastFocusX = focusX;
                mDownFocusY = mLastFocusY = focusY;
                if (mCurrentDownEvent != null) {
                    mCurrentDownEvent.recycle();
                }
                mCurrentDownEvent = MotionEvent.obtain(ev);
                start();
                break;
            }
            case MotionEvent.ACTION_MOVE: {

                final float scrollX = mLastFocusX - focusX;
                final float scrollY = mLastFocusY - focusY;

                final int deltaX = (int) (focusX - mDownFocusX);
                final int deltaY = (int) (focusY - mDownFocusY);
                int distance = (deltaX * deltaX) + (deltaY * deltaY);
                if (distance > mTouchSlopSquare) {
                    if (!isScrolling) {
                        isScrolling = true;
                        mLastFocusX = focusX;
                        mLastFocusY = focusY;
                        deliverHandle();
//                        notifyScrollStart();
                        break;
                    }

                    float scrollXPercent = scrollX / getWidth() * 2f;
                    float scrollYPercent = scrollY / getHeight() * 2f;

                    Log.e(TAG, "scroll: " + scrollX + "/" + scrollY + ", percent: " + scrollXPercent + "/" + scrollYPercent);

                    if (!isScaling) {
                        mRenderer.translate(scrollXPercent, scrollYPercent);
                    }

                    mLastFocusX = focusX;
                    mLastFocusY = focusY;
                }

                break;
            }
            case MotionEvent.ACTION_UP: {

                boolean isFlingState = false;

                MotionEvent currentUpEvent = MotionEvent.obtain(ev);
                if (isScrolling) {
                    final VelocityTracker velocityTracker = mVelocityTracker;
                    final int pointerId = ev.getPointerId(0);
                    velocityTracker.computeCurrentVelocity(1000, mMaximumFlingVelocity);
                    final float velocityY = velocityTracker.getYVelocity(pointerId);
                    final float velocityX = velocityTracker.getXVelocity(pointerId);

                    if ((Math.abs(velocityY) > mMinimumFlingVelocity)
                            || (Math.abs(velocityX) > mMinimumFlingVelocity)) {
                        deliverHandle();
                        isFlingState = true;
                    } else {
                        //TODO:
                    }
                }
                if (mPreviousUpEvent != null) {
                    mPreviousUpEvent.recycle();
                }
                mPreviousUpEvent = currentUpEvent;
                if (mVelocityTracker != null) {
                    mVelocityTracker.recycle();
                    mVelocityTracker = null;
                }
                if (isScrolling) {
                    cancel(isFlingState ? FlingRunnable.FlingType.FLING : FlingRunnable.FlingType.SCROLL);
                } else {
                    final int deltaX = (int) (focusX - mDownFocusX);
                    final int deltaY = (int) (focusY - mDownFocusY);
                    int distance = (deltaX * deltaX) + (deltaY * deltaY);
                    if (distance < mTouchSlopSquare) {
                        performClick();
                    }
                }
                release();
                break;
            }
            case MotionEvent.ACTION_CANCEL: {
                cancel(FlingRunnable.FlingType.SCROLL);
                release();
                break;
            }
        }

        return true;
    }

    private void start() {
        cancelScroller();
    }

    private void cancel(FlingRunnable.FlingType flingType) {
        if (mVelocityTracker != null) {
            mVelocityTracker.recycle();
            mVelocityTracker = null;
        }

        float xOffset = 0f;
        float yOffset = 0f;

        if ((mRenderer.isOutOfTop() && !mRenderer.isOutOfBottom())
                || (!mRenderer.isOutOfTop() && mRenderer.isOutOfBottom())) {
            float topOffset = mRenderer.getTop() - 1f;
            float bottomOffset = mRenderer.getBottom() + 1f;

            if (Math.abs(topOffset) < Math.abs(bottomOffset)) {
                yOffset = topOffset;
            } else {
                yOffset = bottomOffset;
            }
        }

        if ((mRenderer.isOutOfLeft() && !mRenderer.isOutOfRight())
                || (!mRenderer.isOutOfLeft() && mRenderer.isOutOfRight())) {
            float leftOffset = mRenderer.getLeft() + 1f;
            float rightOffset = mRenderer.getRight() - 1f;

            if (Math.abs(leftOffset) < Math.abs(rightOffset)) {
                xOffset = leftOffset;
            } else {
                xOffset = rightOffset;
            }
        }

        ValueAnimator animator = ValueAnimator.ofFloat(0f, 1f);
        final float finalYOffset = yOffset;
        final float finalXOffset = xOffset;
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            float applyedX = 0;
            float applyedY = 0;

            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float xTranslate = finalXOffset * (float) valueAnimator.getAnimatedValue();
                float actX = xTranslate - applyedX;

                float yTranslate = finalYOffset * (float) valueAnimator.getAnimatedValue();
                float actY = yTranslate - applyedY;

                mRenderer.translate(actX, -actY);
                applyedX = xTranslate;
                applyedY = yTranslate;
            }
        });
        animator.start();
    }

    private void release() {
        isScrolling = false;
    }

    private void deliverHandle() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(true);
        }
    }

    private void releaseHandle() {
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestDisallowInterceptTouchEvent(false);
        }
    }

    private void cancelScroller() {
        if (mFlingRunnable != null) {
            mFlingRunnable.cancel();
        }
    }

    public void setSource(Bitmap bitmap) {
        mRenderer.setSource(bitmap);
    }

    public void setScale(float scale) {
        mRenderer.setScale(scale);
    }

    public void getResult(GLImageRenderer.OnStoreListener listener) {
        mRenderer.getResult(listener);
    }

    private ValueAnimator mJumpAnimator;

    private void jumpToScale(float scale) {
        if (mJumpAnimator != null) {
            mJumpAnimator.cancel();
        }
        mJumpAnimator = ValueAnimator.ofFloat(mScale, scale);
        mJumpAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                mScale = (float) valueAnimator.getAnimatedValue();
                mRenderer.setScale(mScale);
            }
        });
        mJumpAnimator.start();
    }

    private float mScale = 1f;
    private boolean isScaling;

    private final ScaleGestureDetector.OnScaleGestureListener mScaleListener
            = new ScaleGestureDetector.OnScaleGestureListener() {


        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            mScale *= scaleGestureDetector.getScaleFactor();
            mScale = Math.max(MIN_OVER_SCALE, Math.min(MAX_OVER_SCALE, mScale));

            mRenderer.setScale(mScale);

            return true;
        }

        @Override
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            isScaling = true;
            return true;
        }

        @Override
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            isScaling = false;
            if (mScale > MAX_SCALE) {
                jumpToScale(MAX_SCALE);
            } else if (mScale < MIN_SCALE) {
                jumpToScale(MIN_SCALE);
            }
        }
    };

    @Override
    public void onSourceLoad(Bitmap bitmap) {
        mRenderer.setSource(bitmap);
        if (mExtraSourceEventListener != null) {
            mExtraSourceEventListener.onSourceLoad(bitmap);
        }
    }

    @Override
    public void onSourceLoadFail(Exception e) {
        if (mExtraSourceEventListener != null) {
            mExtraSourceEventListener.onSourceLoadFail(e);
        }
    }

    public void setOnSourceLoadEventListener(SourceProvider.OnSourceLoadEvent listener) {
        mExtraSourceEventListener = listener;
    }

    public static class FlingRunnable implements Runnable {

        private static final int NOT_SET = -1;

        private View mView;
        private Scroller mScroller;
        private OnFlingProgressListener mListener;

        private boolean isCanceled;

        public enum FlingType {
            SCROLL, FLING, OUT_FLING
        }

        public FlingRunnable(View view, OnFlingProgressListener listener, FlingType type) {
            this(view, listener, new FastOutSlowInInterpolator());
        }

        public FlingRunnable(View view, OnFlingProgressListener listener, Interpolator interpolator) {
            mView = view;
            mListener = listener;
            mScroller = new Scroller(view.getContext(), interpolator);
        }

        void scroll(int xDiffStart, int yDiffStart, int xDiffEnd, int yDiffEnd) {
            scroll(xDiffStart, yDiffStart, xDiffEnd, yDiffEnd, NOT_SET);
        }

        void scroll(int xDiffStart, int yDiffStart, int xDiffEnd, int yDiffEnd, int duration) {
            mScroller.startScroll(xDiffStart, yDiffStart, xDiffEnd, yDiffEnd);
            mScroller.setFinalX(xDiffEnd);
            mScroller.setFinalY(yDiffEnd);
            if (duration != NOT_SET) {
                mScroller.extendDuration(duration);
            }
            ViewCompat.postOnAnimation(mView, this);
        }

        @Override
        public void run() {
            if (!isCanceled) {
                if (!mScroller.isFinished() && mScroller.computeScrollOffset()) {
                    int sx = mScroller.getCurrX();
                    int sy = mScroller.getCurrY();
                    mListener.onScroll(sx, sy);
                    ViewCompat.postOnAnimation(mView, this);
                } else {
                    mListener.onScrollEnd();
                    ViewCompat.postInvalidateOnAnimation(mView);
                }
            }
        }

        public void cancel() {
            isCanceled = true;
            mListener.onScrollCancel();
        }

        public void stop() {
            isCanceled = true;
            mListener.onScrollEnd();
        }

        public interface OnFlingProgressListener {
            void onScroll(int x, int y);

            void onScrollEnd();

            void onScrollCancel();
        }

    }

}
