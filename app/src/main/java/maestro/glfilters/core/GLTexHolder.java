package maestro.glfilters.core;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import jp.co.cyberagent.android.gpuimage.util.TextureRotationUtil;

/**
 * Created by maestro123 on 8/19/16.
 */
public class GLTexHolder {

    public static final String TAG = GLTexHolder.class.getSimpleName();

    private static final float[] TEX_VERTICES = {
            0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
    };

    private static final float[] POS_VERTICES = {
            -1.0f, -1.0f, // V1 - bottom left
            1.0f, -1.0f, // V3 - bottom right
            -1.0f, 1.0f, // V2 - top left
            1.0f, 1.0f  // V4 - top right
    };

    private static final String VERTEX_SHADER =
            "attribute vec4 a_position;\n" +
                    "attribute vec2 a_texcoord;\n" +
                    "varying vec2 v_texcoord;\n" +
                    "void main() {\n" +
                    "  gl_Position = a_position;\n" +
                    "  v_texcoord = a_texcoord;\n" +
                    "}\n";

    private static final String FRAGMENT_SHADER =
            "precision mediump float;\n" +
                    "uniform sampler2D tex_sampler;\n" +
                    "varying vec2 v_texcoord;\n" +
                    "void main() {\n" +
                    "  gl_FragColor = texture2D(tex_sampler, v_texcoord);\n" +
                    "}\n";

    private static final int FLOAT_SIZE_BYTES = 4;

    public FloatBuffer posVertices;
    public FloatBuffer texVertices;
    public FloatBuffer texVerticesFlip;

    private float[] mTexCords;
    private float[] mPosCords;
    private float[] mTextFlipCords;

    private int texCoordHandle;
    private int posCoordHandle;

    private int shaderProgram;
    private int texSamplerHandle;

    private int mViewportWidth;
    private int mViewportHeight;

    private int mTargetWidth;
    private int mTargetHeight;

    private float scale = 1f;
    private float xOffset;
    private float yOffset;

    public GLTexHolder() {
        texVertices = ByteBuffer.allocateDirect(TEX_VERTICES.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        posVertices = ByteBuffer.allocateDirect(POS_VERTICES.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        texVerticesFlip = ByteBuffer.allocateDirect(TEX_VERTICES.length * FLOAT_SIZE_BYTES)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
    }

    public void setViewPortSize(int width, int height) {
        mViewportWidth = width;
        mViewportHeight = height;
    }

    public void setTargetSize(int width, int height, boolean centerCrop) {
        mTargetWidth = width;
        mTargetHeight = height;
        adjustScaling(centerCrop);
    }

    public void scale(float scale) {
        this.scale = scale;
        updateVertices();
    }

    public void translate(float xOffset, float yOffset) {
        this.xOffset = -xOffset;
        this.yOffset = yOffset;
        updateVertices();
    }

    private float[] updateVertices() {

        float[] posCords = new float[mPosCords.length];
        System.arraycopy(mPosCords, 0, posCords, 0, mPosCords.length);

        posCords[0] *= scale * aditScale;
        posCords[1] *= scale * aditScale;
        posCords[2] *= scale * aditScale;
        posCords[3] *= scale * aditScale;
        posCords[4] *= scale * aditScale;
        posCords[5] *= scale * aditScale;
        posCords[6] *= scale * aditScale;
        posCords[7] *= scale * aditScale;

        posCords[0] += xOffset;
        posCords[2] += xOffset;
        posCords[4] += xOffset;
        posCords[6] += xOffset;

        posCords[1] += yOffset;
        posCords[3] += yOffset;
        posCords[5] += yOffset;
        posCords[7] += yOffset;

        posVertices.clear();
        posVertices.put(posCords).position(0);
        return posCords;
    }

    private float aditScale = 1f;

    boolean isCenterCrop;

    private void adjustScaling(boolean centerCrop) {

        isCenterCrop = centerCrop;

        float outputWidth = mViewportWidth;
        float outputHeight = mViewportHeight;

        float ratio1 = outputWidth / mTargetWidth;
        float ratio2 = outputHeight / mTargetHeight;
        float ratioMax = Math.max(ratio1, ratio2);
        int imageWidthNew = Math.round(mTargetWidth * ratioMax);
        int imageHeightNew = Math.round(mTargetHeight * ratioMax);

        float ratioWidth = imageWidthNew / outputWidth;
        float ratioHeight = imageHeightNew / outputHeight;

        float[] texCords = TextureRotationUtil.TEXTURE_NO_ROTATION;
        if (centerCrop) {
            aditScale = Math.max(ratioWidth, ratioHeight);
        }

        float[] posCords = new float[]{
                POS_VERTICES[0] / ratioHeight, POS_VERTICES[1] / ratioWidth,
                POS_VERTICES[2] / ratioHeight, POS_VERTICES[3] / ratioWidth,
                POS_VERTICES[4] / ratioHeight, POS_VERTICES[5] / ratioWidth,
                POS_VERTICES[6] / ratioHeight, POS_VERTICES[7] / ratioWidth,
        };

        mTexCords = texCords;
        mTextFlipCords = new float[]{
                texCords[0], flip(texCords[1]),
                texCords[2], flip(texCords[3]),
                texCords[4], flip(texCords[5]),
                texCords[6], flip(texCords[7]),
        };

        texVertices.clear();
        texVertices.put(mTexCords).position(0);
        texVerticesFlip.clear();
        texVerticesFlip.put(mTextFlipCords).position(0);

        mPosCords = new float[posCords.length];
        System.arraycopy(posCords, 0, mPosCords, 0, posCords.length);
        updateVertices();

    }

    public float getLeft() {
        return posVertices.get(0);
    }

    public float getRight() {
        return posVertices.get(2);
    }

    public float getTop() {
        return posVertices.get(5);
    }

    public float getBottom() {
        return posVertices.get(3);
    }

    //???

    public float getDiffLeft() {
        return diff(mPosCords[0], posVertices.get(0));
    }

    public float getDiffRight() {
        return diff(mPosCords[2], posVertices.get(2));
    }

    public float getDiffTop() {
        return diff(mPosCords[5], posVertices.get(5));
    }

    public float getDiffBottom() {
        return diff(mPosCords[1], posVertices.get(1));
    }

    public float getTotalScale() {
        return aditScale * scale;
    }

    private float diff(float f1, float f2) {
        return (Math.abs(Math.abs(f1) - Math.abs(f2)));
    }

    public static GLTexHolder create() {
        GLTexHolder holder = new GLTexHolder();

        final int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
        final int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);

        int program = GLES20.glCreateProgram();
        if (program != 0) {
            GLES20.glAttachShader(program, vertexShader);
            GLHelper.checkGlError("glAttachShader");
            GLES20.glAttachShader(program, pixelShader);
            GLHelper.checkGlError("glAttachShader");
            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                String info = GLES20.glGetProgramInfoLog(program);
                GLES20.glDeleteProgram(program);
                program = 0;
                throw new RuntimeException("Could not link program: " + info);
            }
        }
        // Bind attributes and uniforms
        holder.texSamplerHandle = GLES20.glGetUniformLocation(program, "tex_sampler");
        holder.texCoordHandle = GLES20.glGetAttribLocation(program, "a_texcoord");
        holder.posCoordHandle = GLES20.glGetAttribLocation(program, "a_position");
        holder.shaderProgram = program;
        return holder;
    }

    public static void renderTexture(GLTexHolder holder, int texture, boolean flip) {

        GLES20.glUseProgram(holder.shaderProgram);
        GLHelper.checkGlError("glUseProgram");

        GLES20.glViewport(0, 0, holder.mViewportWidth, holder.mViewportHeight);
        GLHelper.checkGlError("glViewport");

        GLES20.glDisable(GLES20.GL_BLEND);

        FloatBuffer texBuffer;
        if (flip) {
            texBuffer = holder.texVerticesFlip;
        } else {
            texBuffer = holder.texVertices;
        }

        holder.texVertices.position(0);
        GLES20.glVertexAttribPointer(holder.texCoordHandle, 2, GLES20.GL_FLOAT, false, 0, texBuffer);
        GLES20.glEnableVertexAttribArray(holder.texCoordHandle);

        holder.posVertices.position(0);
        GLES20.glVertexAttribPointer(holder.posCoordHandle, 2, GLES20.GL_FLOAT, false, 0, holder.posVertices);
        GLES20.glEnableVertexAttribArray(holder.posCoordHandle);
        GLHelper.checkGlError("vertex attribute setup");

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLHelper.checkGlError("glActiveTexture");
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLHelper.checkGlError("glBindTexture");
        GLES20.glUniform1i(holder.texSamplerHandle, 0);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLHelper.checkGlError("glDrawArrays");
    }

    private static int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        if (shader != 0) {
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);
            int[] compiled = new int[1];
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                String info = GLES20.glGetShaderInfoLog(shader);
                GLES20.glDeleteShader(shader);
                shader = 0;
                throw new RuntimeException("Could not compile shader " + shaderType + ":" + info);
            }
        }
        return shader;
    }

    private static float flip(final float i) {
        if (i == 0.0f) {
            return 1.0f;
        }
        return 0.0f;
    }

}
