package maestro.glfilters.core;

import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageKuwaharaFilter;

/**
 * Created by maestro123 on 19.08.2016.
 */
public class GPUEffectsProvider implements IEffectsProvider {

    private ArrayList<GPUImageFilter> mFilters = new ArrayList<>();

    public GPUEffectsProvider(){
        mFilters.add(new GPUImageKuwaharaFilter(6));
    }

    public void addEffect(GPUImageFilter filter) {
        mFilters.add(filter);
    }

    @Override
    public void draw(GLTexHolder holder, int originTexture, int outTexture, int sourceWidth, int sourceHeight) {
//        GLHelper.copyTexToTex(originTexture, outTexture, sourceWidth, sourceHeight);
        for (GPUImageFilter filter : mFilters) {
            filter.onInit();
            filter.onInitialized();
//            filter.onDraw(outTexture, holder.posVertices, holder.texVertices);
            filter.onDraw(originTexture, holder.posVertices, holder.texVertices);
        }
    }

    @Override
    public void clear() {

    }

    @Override
    public void addEffect(String effect) {

    }

    @Override
    public boolean haveEffects() {
        return mFilters.size() > 0;
    }
}
