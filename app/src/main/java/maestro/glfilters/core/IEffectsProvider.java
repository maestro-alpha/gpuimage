package maestro.glfilters.core;

/**
 * Created by maestro123 on 19.08.2016.
 */
public interface IEffectsProvider {

    void draw(GLTexHolder holder, int originTexture, int outTexture, int sourceWidth, int sourceHeight);

    void clear();

    void addEffect(String effect);

    boolean haveEffects();

}
