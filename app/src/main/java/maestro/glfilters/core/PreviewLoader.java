package maestro.glfilters.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL10;

import maestro.glfilters.R;

import static javax.microedition.khronos.egl.EGL10.EGL_ALPHA_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_BLUE_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_DEFAULT_DISPLAY;
import static javax.microedition.khronos.egl.EGL10.EGL_DEPTH_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_GREEN_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_HEIGHT;
import static javax.microedition.khronos.egl.EGL10.EGL_NONE;
import static javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT;
import static javax.microedition.khronos.egl.EGL10.EGL_RED_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_STENCIL_SIZE;
import static javax.microedition.khronos.egl.EGL10.EGL_WIDTH;

/**
 * Created by maestro123 on 8/21/16.
 */
public class PreviewLoader {

    public static final String TAG = PreviewLoader.class.getSimpleName();

    private final ArrayList<WorkTask> mActiveTasks = new ArrayList<>();

    private int mPreviewSize;

    public PreviewLoader(Context context) {
        mPreviewSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, context.getResources().getDisplayMetrics());
    }

    public void load(ImageView imageView, String source, String effect,
                     SourceProvider sourceProvider, IEffectsProvider effectsProvider, LoadListener listener) {

        String key = source + "_ef_" + effect;

        Bitmap cached = Cache.get().get(key);
        if (cached != null) {
            cancelWorker(imageView, key);
            submitResult(imageView, listener, cached);
            return;
        }

        if (cancelWorker(imageView, key)) {
            WorkTask task = new WorkTask(key, effect, imageView, listener, sourceProvider, effectsProvider);
            imageView.setTag(R.id.GpuPreviewTask, task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    public void cancel(ImageView imageView) {
        WorkTask task = (WorkTask) imageView.getTag(R.id.GpuPreviewTask);
        if (task != null) {
            task.cancel(true);
        }
    }

    private void submitResult(View imageView, LoadListener listener, Bitmap bitmap) {
        setResult(imageView, bitmap);
        if (listener != null) {
            listener.onLoadFinished(bitmap);
        }
    }

    private boolean cancelWorker(View imageView, String key) {
        WorkTask task = (WorkTask) imageView.getTag(R.id.GpuPreviewTask);
        if (task != null) {
            if (task.getKey().equals(key)) {
                return false;
            }
            task.cancel(true);
            setResult(imageView, null);
        }
        return true;
    }

    private void setResult(View view, Bitmap bitmap) {
        if (view instanceof ImageView) {
            ((ImageView) view).setImageBitmap(bitmap);
        }
    }

    private class WorkTask extends AsyncTask<Object, Object, Bitmap> {

        private String mKey;
        private String mEffect;
        private IEffectsProvider mEffectsProvider;

        private WeakReference<View> imageReference;
        private WeakReference<LoadListener> listenerReference;

        private WeakReference<SourceProvider> sourceProviderReference;

        public WorkTask(String key, String effect, View imageView, LoadListener listener, SourceProvider sourceProvider, IEffectsProvider effectsProvider) {
            mKey = key;
            mEffect = effect;
            mEffectsProvider = effectsProvider;
            imageReference = new WeakReference<View>(imageView);
            listenerReference = new WeakReference<LoadListener>(listener);
            sourceProviderReference = new WeakReference<SourceProvider>(sourceProvider);
        }

        public String getKey() {
            return mKey;
        }

        public String getEffect() {
            return mEffect;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mActiveTasks.add(this);
            LoadListener listener = listenerReference.get();
            if (listener != null) {
                listener.onLoadStart();
            }
        }

        @Override
        protected Bitmap doInBackground(Object... objects) {
            Bitmap bitmap = getSourceBitmap(mKey);
            if (bitmap != null) {
                GLImageRenderer renderer = new GLImageRenderer(null);
                renderer.setSource(bitmap);
                renderer.setEffectsProvider(mEffectsProvider);

                PixelBuffer pixelBuffer = new PixelBuffer(mPreviewSize, mPreviewSize);
                pixelBuffer.setRenderer(renderer);
                try {
                    pixelBuffer.draw();
                    Bitmap result = GLHelper.saveTexture(renderer.getDrawHolder(), renderer.getDisplayTexture(), mPreviewSize, mPreviewSize);
                    if (result != null) {
                        Bitmap rgb565result = result.copy(Bitmap.Config.RGB_565, false);
                        result.recycle();
                        Cache.get().add(mKey, rgb565result);
                        return rgb565result;
                    }
                } finally {
                    pixelBuffer.destroy();
                }
            }
            return null;
        }

        private final Object mLock = new Object();
        final CountDownLatch mLatch = new CountDownLatch(1);

        private Bitmap getSourceBitmap(String source) {
            synchronized (mLock) {
                Bitmap cached = Cache.get().getPreview(source);
                if (cached != null) {
                    return cached;
                }
                final SourceProvider.OnSourceLoadEvent listener = new SourceProvider.OnSourceLoadEvent() {
                    @Override
                    public void onSourceLoad(Bitmap bitmap) {
                        Log.e(TAG, "onSourceLoad: " + bitmap);
                        mLatch.countDown();
                    }

                    @Override
                    public void onSourceLoadFail(Exception e) {
                        Log.e(TAG, "onSourceLoadFail: " + e);
                        mLatch.countDown();
                    }
                };
                SourceProvider provider = sourceProviderReference.get();
                if (provider != null) {
                    if (provider.isLoading()) {
                        provider.addListener(listener);
                        try {
                            Log.e(TAG, "lock");
                            mLatch.await();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Bitmap result = provider.getResult();
                    if (result != null) {
                        result = GLHelper.scaleCenterCrop(result, mPreviewSize, mPreviewSize);
                        Cache.get().addPreview(source, result);
                        return result;
                    }
                }
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap drawable) {
            super.onPostExecute(drawable);
            mActiveTasks.remove(this);
            if (!isCancelled()) {
                View imageView = imageReference.get();
                if (imageView != null) {
                    submitResult(imageView, listenerReference.get(), drawable);
                } else {
                    LoadListener listener = listenerReference.get();
                    if (listener != null) {
                        listener.onLoadFinished(drawable);
                    }
                }
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (imageReference != null) {
                imageReference.clear();
            }
            if (listenerReference != null) {
                listenerReference.clear();
            }
        }
    }

    public void clean() {
        Cache.get().clean();
        for (WorkTask task : mActiveTasks) {
            task.cancel(true);
        }
    }

    public interface LoadListener {
        void onLoadStart();

        void onLoadFinished(Bitmap bitmap);
    }

    private static final class Cache {

        private static volatile Cache instance;

        private final LruCache<String, Bitmap> mPreviewCache;
        private final LruCache<String, Bitmap> mCache;

        public synchronized static Cache get() {
            if (instance == null) {
                synchronized (Cache.class) {
                    if (instance == null) {
                        instance = new Cache();
                    }
                }
            }
            return instance;
        }

        Cache() {
            mCache = new LruCache<String, Bitmap>(10 * 1024 * 1024) {
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    if (value != null) {
                        return value.getByteCount();
                    }
                    return 0;
                }
            };
            mPreviewCache = new LruCache<String, Bitmap>(5 * 1024 * 1024) {
                @Override
                protected int sizeOf(String key, Bitmap value) {
                    if (value != null) {
                        return value.getByteCount();
                    }
                    return 0;
                }
            };
        }

        public void add(String key, Bitmap bitmap) {
            Log.e(TAG, "add: " + key + ", size: " + convert(mCache.size(), false) + ", max: " + convert(mCache.maxSize(), false));
            synchronized (mCache) {
                mCache.put(key, bitmap);
            }
        }

        public static String convert(int bytes, boolean si) {
            int unit = si ? 1000 : 1024;
            if (bytes < unit) return bytes + " B";
            int exp = (int) (Math.log(bytes) / Math.log(unit));
            String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
            return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
        }

        public void addPreview(String key, Bitmap bitmap) {
            synchronized (mPreviewCache) {
                mPreviewCache.put(key, bitmap);
            }
        }

        public Bitmap get(String key) {
            Log.e(TAG, "get: " + key);
            synchronized (mCache) {
                Bitmap bitmap = mCache.get(key);
                if (bitmap == null || bitmap.isRecycled()) {
                    removeKey(key);
                    return null;
                }
                return bitmap;
            }
        }

        public Bitmap getPreview(String key) {
            synchronized (mPreviewCache) {
                return mPreviewCache.get(key);
            }
        }

        public void removeKey(String key) {
            synchronized (mCache) {
                mCache.remove(key);
            }
        }

        public void removePreview(String key) {
            synchronized (mPreviewCache) {
                mPreviewCache.remove(key);
            }
        }

        public void clean() {
            synchronized (mCache) {
                mCache.evictAll();
            }
            synchronized (mPreviewCache) {
                mPreviewCache.evictAll();
            }
        }

    }

    public class PixelBuffer {

        GLSurfaceView.Renderer mRenderer;

        EGL10 mEGL;
        EGLDisplay mEGLDisplay;
        EGLConfig[] mEGLConfigs;
        EGLConfig mEGLConfig;
        EGLContext mEGLContext;
        EGLSurface mEGLSurface;
        GL10 mGL;

        int mWidth, mHeight;

        public PixelBuffer(final int width, final int height) {
            mWidth = width;
            mHeight = height;
            int[] version = new int[2];
            int[] attribList = new int[]{
                    EGL_WIDTH, mWidth,
                    EGL_HEIGHT, mHeight,
                    EGL_NONE
            };
            mEGL = (EGL10) EGLContext.getEGL();
            mEGLDisplay = mEGL.eglGetDisplay(EGL_DEFAULT_DISPLAY);
            mEGL.eglInitialize(mEGLDisplay, version);
            mEGLConfig = chooseConfig();
            int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
            int[] attrib_list = {
                    EGL_CONTEXT_CLIENT_VERSION, 2,
                    EGL10.EGL_NONE
            };
            mEGLContext = mEGL.eglCreateContext(mEGLDisplay, mEGLConfig, EGL_NO_CONTEXT, attrib_list);
            mEGLSurface = mEGL.eglCreatePbufferSurface(mEGLDisplay, mEGLConfig, attribList);
            mEGL.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext);
            mGL = (GL10) mEGLContext.getGL();
        }

        public void setRenderer(final GLSurfaceView.Renderer renderer) {
            mRenderer = renderer;
            mRenderer.onSurfaceCreated(mGL, mEGLConfig);
            mRenderer.onSurfaceChanged(mGL, mWidth, mHeight);
        }

        public void draw() {
            mRenderer.onDrawFrame(mGL);
        }

        public void destroy() {
            mEGL.eglMakeCurrent(mEGLDisplay, EGL10.EGL_NO_SURFACE,
                    EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            mEGL.eglDestroySurface(mEGLDisplay, mEGLSurface);
            mEGL.eglDestroyContext(mEGLDisplay, mEGLContext);
            mEGL.eglTerminate(mEGLDisplay);
        }

        private EGLConfig chooseConfig() {
            int[] attribList = new int[]{
                    EGL_DEPTH_SIZE, 0,
                    EGL_STENCIL_SIZE, 0,
                    EGL_RED_SIZE, 8,
                    EGL_GREEN_SIZE, 8,
                    EGL_BLUE_SIZE, 8,
                    EGL_ALPHA_SIZE, 8,
                    EGL10.EGL_RENDERABLE_TYPE, 4,
                    EGL_NONE
            };

            int[] numConfig = new int[1];
            mEGL.eglChooseConfig(mEGLDisplay, attribList, null, 0, numConfig);
            int configSize = numConfig[0];
            mEGLConfigs = new EGLConfig[configSize];
            mEGL.eglChooseConfig(mEGLDisplay, attribList, mEGLConfigs, configSize, numConfig);
            return mEGLConfigs[0];
        }
    }

//    public class PixelBuffer {
//        final static String TAG = "PixelBuffer";
//        final static boolean LIST_CONFIGS = false;
//
//        GLSurfaceView.Renderer mRenderer; // borrow this interface
//        int mWidth, mHeight;
//        Bitmap mBitmap;
//
//        EGL10 mEGL;
//        EGLDisplay mEGLDisplay;
//        EGLConfig[] mEGLConfigs;
//        EGLConfig mEGLConfig;
//        EGLContext mEGLContext;
//        EGLSurface mEGLSurface;
//        GL10 mGL;
//
//        String mThreadOwner;
//
//        public PixelBuffer(final int width, final int height) {
//            mWidth = width;
//            mHeight = height;
//
//            int[] version = new int[2];
//            int[] attribList = new int[]{
//                    EGL_WIDTH, mWidth,
//                    EGL_HEIGHT, mHeight,
//                    EGL_NONE
//            };
//
//            // No error checking performed, minimum required code to elucidate logic
//            mEGL = (EGL10) EGLContext.getEGL();
//            mEGLDisplay = mEGL.eglGetDisplay(EGL_DEFAULT_DISPLAY);
//            mEGL.eglInitialize(mEGLDisplay, version);
//            mEGLConfig = chooseConfig(); // Choosing a config is a little more
//            // complicated
//
//            // mEGLContext = mEGL.eglCreateContext(mEGLDisplay, mEGLConfig,
//            // EGL_NO_CONTEXT, null);
//            int EGL_CONTEXT_CLIENT_VERSION = 0x3098;
//            int[] attrib_list = {
//                    EGL_CONTEXT_CLIENT_VERSION, 2,
//                    EGL10.EGL_NONE
//            };
//            mEGLContext = mEGL.eglCreateContext(mEGLDisplay, mEGLConfig, EGL_NO_CONTEXT, attrib_list);
//
//            mEGLSurface = mEGL.eglCreatePbufferSurface(mEGLDisplay, mEGLConfig, attribList);
//            mEGL.eglMakeCurrent(mEGLDisplay, mEGLSurface, mEGLSurface, mEGLContext);
//
//            mGL = (GL10) mEGLContext.getGL();
//
//            // Record thread owner of OpenGL context
//            mThreadOwner = Thread.currentThread().getName();
//        }
//
//        public void setRenderer(final GLSurfaceView.Renderer renderer) {
//            mRenderer = renderer;
//
//            // Does this thread own the OpenGL context?
//            if (!Thread.currentThread().getName().equals(mThreadOwner)) {
//                Log.e(TAG, "setRenderer: This thread does not own the OpenGL context.");
//                return;
//            }
//
//            // Call the renderer initialization routines
//            mRenderer.onSurfaceCreated(mGL, mEGLConfig);
//            mRenderer.onSurfaceChanged(mGL, mWidth, mHeight);
//        }
//
//        public void draw() {
//            // Do we have a renderer?
//            if (mRenderer == null) {
//                Log.e(TAG, "getBitmap: Renderer was not set.");
//                return;
//            }
//
//            // Does this thread own the OpenGL context?
//            if (!Thread.currentThread().getName().equals(mThreadOwner)) {
//                Log.e(TAG, "getBitmap: This thread does not own the OpenGL context.");
//                return;
//            }
//
//            // Call the renderer draw routine (it seems that some filters do not
//            // work if this is only called once)
//            mRenderer.onDrawFrame(mGL);
////            mRenderer.onDrawFrame(mGL);
//        }
//
//        public void destroy() {
//            mEGL.eglMakeCurrent(mEGLDisplay, EGL10.EGL_NO_SURFACE,
//                    EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
//
//            mEGL.eglDestroySurface(mEGLDisplay, mEGLSurface);
//            mEGL.eglDestroyContext(mEGLDisplay, mEGLContext);
//            mEGL.eglTerminate(mEGLDisplay);
//        }
//
//        private EGLConfig chooseConfig() {
//            int[] attribList = new int[]{
//                    EGL_DEPTH_SIZE, 0,
//                    EGL_STENCIL_SIZE, 0,
//                    EGL_RED_SIZE, 8,
//                    EGL_GREEN_SIZE, 8,
//                    EGL_BLUE_SIZE, 8,
//                    EGL_ALPHA_SIZE, 8,
//                    EGL10.EGL_RENDERABLE_TYPE, 4,
//                    EGL_NONE
//            };
//
//            // No error checking performed, minimum required code to elucidate logic
//            // Expand on this logic to be more selective in choosing a configuration
//            int[] numConfig = new int[1];
//            mEGL.eglChooseConfig(mEGLDisplay, attribList, null, 0, numConfig);
//            int configSize = numConfig[0];
//            mEGLConfigs = new EGLConfig[configSize];
//            mEGL.eglChooseConfig(mEGLDisplay, attribList, mEGLConfigs, configSize, numConfig);
//
//            if (LIST_CONFIGS) {
//                listConfig();
//            }
//
//            return mEGLConfigs[0]; // Best match is probably the first configuration
//        }
//
//        private void listConfig() {
//            Log.i(TAG, "Config List {");
//
//            for (EGLConfig config : mEGLConfigs) {
//                int d, s, r, g, b, a;
//
//                // Expand on this logic to dump other attributes
//                d = getConfigAttrib(config, EGL_DEPTH_SIZE);
//                s = getConfigAttrib(config, EGL_STENCIL_SIZE);
//                r = getConfigAttrib(config, EGL_RED_SIZE);
//                g = getConfigAttrib(config, EGL_GREEN_SIZE);
//                b = getConfigAttrib(config, EGL_BLUE_SIZE);
//                a = getConfigAttrib(config, EGL_ALPHA_SIZE);
//                Log.i(TAG, "    <d,s,r,g,b,a> = <" + d + "," + s + "," +
//                        r + "," + g + "," + b + "," + a + ">");
//            }
//
//            Log.i(TAG, "}");
//        }
//
//        private int getConfigAttrib(final EGLConfig config, final int attribute) {
//            int[] value = new int[1];
//            return mEGL.eglGetConfigAttrib(mEGLDisplay, config,
//                    attribute, value) ? value[0] : 0;
//        }
//    }
}
