package maestro.glfilters.core;

import android.content.Context;
import android.graphics.Bitmap;

import java.lang.ref.WeakReference;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by maestro123 on 19.08.2016.
 */
public abstract class SourceProvider {

    private WeakReference<Bitmap> mSource;
    private CopyOnWriteArrayList<WeakReference<OnSourceLoadEvent>> mListeners = new CopyOnWriteArrayList<>();

    private boolean isLoading = true;

    //Note: context will shared, so use application context
    public void load(final Context context, OnSourceLoadEvent listener) {
        isLoading = true;
        mListeners.add(new WeakReference<OnSourceLoadEvent>(listener));
        new Thread() {
            @Override
            public void run() {
                super.run();
                Bitmap result = null;
                Exception exception = null;
                try {
                    result = obtain(context);
                } catch (Exception throwable) {
                    throwable.printStackTrace();
                    exception = throwable;
                }
                for (WeakReference<OnSourceLoadEvent> listenerReference : mListeners) {
                    OnSourceLoadEvent listener = listenerReference.get();
                    if (result != null) {
                        mSource = new WeakReference<Bitmap>(result);
                    }
                    if (listener != null) {
                        if (result == null) {
                            listener.onSourceLoadFail(exception);
                        } else {
                            listener.onSourceLoad(result);
                        }
                    }
                }
                isLoading = false;
            }
        }.start();
    }

    public void addListener(OnSourceLoadEvent listener) {
        mListeners.add(new WeakReference<OnSourceLoadEvent>(listener));
    }

    public Bitmap getResult() {
        if (mSource != null) {
            return mSource.get();
        }
        return null;
    }

    public void release() {
        if (mSource != null) {
            if (mSource.get() != null) {
                mSource.get().recycle();
            }
            mSource.clear();
            mSource = null;
        }
    }

    public boolean isLoading() {
        return isLoading;
    }

    protected abstract Bitmap obtain(Context context) throws Exception;

    public interface OnSourceLoadEvent {
        void onSourceLoad(Bitmap bitmap);

        void onSourceLoadFail(Exception e);
    }

}
